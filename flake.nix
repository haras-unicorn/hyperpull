{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.11";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils }:
    utils.lib.simpleFlake {
      inherit self nixpkgs;
      name = "hyperpull";
      config = {
        allowUnfree = true;
      };
      shell = { pkgs }:
        pkgs.mkShell {
          packages = with pkgs;
          [
            # Nix
            nil
            nixpkgs-fmt

            # Godot
            godot_4

            # Rust
            llvmPackages.clangNoLibcxx
            llvmPackages.lldb
            rustc
            cargo
            clippy
            rustfmt
            rust-analyzer
            cargo-edit

            # Misc
            just
            nodePackages.prettier
            nodePackages.yaml-language-server
            nodePackages.vscode-langservers-extracted
            marksman
            taplo
          ];
        };
    };
}
